import { Action } from 'redux';

export interface CardClickType extends Action {
  type: 'CARD_CLICK';
  payload: {
    cid: string;
  };
}

export type CardActions = CardClickType;
