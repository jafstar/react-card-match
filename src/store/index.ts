import { combineReducers, Reducer, Dispatch } from 'redux';

import cardsReducer from './cards/reducers';
import deckReducer from './deck/reducers';

import { CardState } from '../models';

export interface AppState {
  cards: CardState;
}

export const reducers: Reducer<AppState> = combineReducers<AppState>({
  cards: cardsReducer,
  deck: deckReducer
});

/**
 * mapDispatchToProps...
 */
export interface ConnectedReduxProps<S> {
  dispatch: Dispatch<S>;
}
