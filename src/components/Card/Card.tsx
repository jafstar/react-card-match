import * as React from 'react';
import styled from 'styled-components';

import { CardProps, CardState } from '../../models';
import CardSideBack from './CardSideBack';
import CardSideFront from './CardSideFront';
// import * as DeckFunc from "../Deck/DecFunc";

const CardContainer = styled.div`
  border: 1px solid #999;
  background-color: #fff;
  width: 60px;
  height: 90px;
  border-radius: 5px;
  margin: 5px 5px 10px 5px;
  float: left;
  position: relative;
  cursor: pointer;
  overflow: hidden;
  color: #999;
  padding: 0px;
  text-decoration: none;

  .active {
    transform: rotateY(0deg);
    transform-style: preserve-3d;
    transition: all 0.5s ease-in-out;
  }

  .inactive {
    transform: rotateY(180deg);
    transform-style: preserve-3d;
    transition: all 0.5s ease-in-out;
  }
`;

class Card extends React.Component<CardProps, CardState> {
   
  static getDerivedStateFromProps(nextProps: any, prevState: any) {
    // tslint:disable-next-line:no-console
    // console.log(`${nextProps} ${prevState}`);
    return {
      cid: nextProps.cid,
      frontVisible: nextProps.flipped ? true : false,
      backVisible: nextProps.flipped ? false : true,
      isFlipped: nextProps.flipped
    };
  }
 
  constructor(props: CardProps) {
    super(props);

    this.state = {
      cid: this.props.cid,
      frontVisible: this.props.flipped ? true : false,
      backVisible: this.props.flipped ? false : true,
      isFlipped: this.props.flipped
    };
  }

  public handleClick = (evt: any): void => {

    this.props.handleClick(this.props.cid);

    // this.setState({
     // frontVisible: this.props.flipped ? true : false,
     // backVisible: this.props.flipped ? false : true,
     // frontVisible: !this.state.frontVisible,
     // backVisible: !this.state.backVisible
    // });
  }

  public render() {
    return (
      <CardContainer
        id={this.props.cid}
        className={this.state.frontVisible ? 'active' : 'inactive'}
        onClick={this.handleClick}
      >
        <CardSideFront
          className="cardFront"
          visible={this.state.frontVisible}
          num={String(this.props.num)}
          suit={String(this.props.suit)}
          color={String(this.props.color)}
        />
        <CardSideBack className="cardBack" visible={this.state.backVisible} />
      </CardContainer>
    );
  }
}

export default Card;
