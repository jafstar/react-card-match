import { CardProps } from '../../models';

/**
 * CREATE
 */
function createDeck(): Array<CardProps> {
  let theDeck: Array<CardProps> = [];
  let theSuits: Array<string> = ['♣', '♠', '♥', '♦'];
  let theNums = [
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    'J',
    'Q',
    'K',
    'A'
  ];

  theSuits.map(suit => {
    theNums.map(num => {
      let _color: string = suit === '♥' || suit === '♦' ? 'red' : 'black';
      let _sym = convertCardSymbolToString(suit);
      let _uid = num + '-' + _sym;

      theDeck.push({
        num: num,
        suit: suit,
        color: _color,
        cid: _uid,
        sym: _sym,
        key: _uid,
        flipped: false,
        handleClick: null
      });
    });
  });

  return shuffle(theDeck);
}

/**
 * SHUFFLE - 'FISHER-YATES'
 */
function shuffle(arr: Array<CardProps>): Array<CardProps> {
  let ctr = arr.length;

  while (ctr > 0) {
    let idx = Math.floor(Math.random() * ctr);

    ctr--;

    // SWAP
    let tmp = arr[ctr];
    arr[ctr] = arr[idx];
    arr[idx] = tmp;
  }

  return arr;
}

/**
 * CONVERT
 */
function convertCardSymbolToString(sym: string): string {
  let convertedSymbol: string = '';

  switch (sym) {
    case '♣':
      convertedSymbol = 'C';
      break;
    case '♠':
      convertedSymbol = 'S';
      break;
    case '♥':
      convertedSymbol = 'H';
      break;
    case '♦':
      convertedSymbol = 'D';
      break;
    default:
      break;
  }

  return convertedSymbol;
}

export { createDeck, convertCardSymbolToString, shuffle };
